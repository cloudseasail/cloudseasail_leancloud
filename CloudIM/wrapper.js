//var AV = require('leanengine');
var Conversation = require("./conversation")
var UserHelper = require("./user")
var Push = require("./Push")
var AutoReply  = require("./AutoReply")
var Robot = require("./Robot")
var Author = require("./Author")

extend(exports,{
	createConversation : Conversation.create,
	getHistoryMessages:  Conversation.getLog,
	sendMsg: sendMsg,
	syncConversationList: Conversation.searchUserList,
	newUserRegistration: newUserRegistration,
})




/*msg_payload: Message*/
function sendMsg(msg_payload, done, auto){
	

	
	/*send to conv*/
	Conversation.sendMsg(msg_payload,function(msg){
		
		UserHelper.getById(msg.from_id, function(user){
			
			msg.timestamp = new Date().getTime();
			
			var _push_msg = {
				peerUser: user,
				msgPayload: msg,
			}
			
			/*push to*/
			pushMsg(_push_msg);
			
		
			/*update latest*/
			Conversation.saveLatestMsg(msg);
			done&& done(_push_msg);
			
			/*auto reply*/
			if(!auto){
				AutoReply.checkAutoReply(msg, function(reply_msg){
					sendMsg(reply_msg, null, true)
				})
			}
		})
		
	})
	
	
}

function pushMsg(push_msg){
	UserHelper.getById(push_msg.msgPayload.to_id, function(user){
		if(user && user.pushId && user.pushId!= ""){

			Push.pushSingle({
				pushId: user.pushId,
				payload: push_msg,
			})
			
		}
		
	})
}

function newUserRegistration(user){
	Conversation.create({
		name: "Robot Room",
		m: [Robot.userId, user.userId],
	}, function(conv){
		var hello = Robot.getHelloMsg(conv.conv_id, user.userId);
		sendMsg(hello);
	})
	
	Conversation.create({
		name: "Author Room",
		m: [Author.userId, user.userId],
	}, function(conv){
		var hello = Author.getHelloMsg(conv.conv_id, user.userId);
		sendMsg(hello);
	})
}

function extend(target, value){
	for(var p in value){
		target[p] = value[p];
	}
}