var AVHelper = require("../common/AVHelper")
var AV = AVHelper.getRawAV()
extend(exports,{
	create : create,
	getLog:  getConvLog,
	sendMsg: send,
	searchUserList: syncConversationList,
	saveLatestMsg: saveLatestMsg,
})

var ConvClass = AVHelper.use( "_Conversation");
var RestHelper = AVHelper.RestHelper;


/*name, m*/
function create(o, done){
		
		RestHelper.post("/classes/_Conversation", o, function(xhr, status){
			
			if(status == "success"){
				o.conv_id = xhr.objectId;
				done&& done(o)
			}

		})
	}
	
function send(msg, done){

	var params = {
		from_peer: msg.from_id,
		conv_id: msg.conv_id,
		transient: false,
		message: msg.data,
		no_sync: true,
		wait: false,
	}
	
	/*master key*/
	RestHelper.post("/rtm/messages", params, function(xhr, status){
		if(status == "success"){
			done&& done(msg)
		}

	}, true)
}

function getConvLog(o , done){
	var params = {
		convid: o.conv_id,
//			max_ts: ,
		limit: o.limit || 100,
//			from: o.from,
		
	}
	RestHelper.get("/rtm/messages/logs", params, function(xhr, status){
		if(status == "success"){
			done&& done(xhr)
		}

		
	})
	
}

function getConvFrom(o, done){
	var params = {
		limit: o.limit || 100,
		from: o.from,
		
	}
	RestHelper.get("/rtm/messages/logs", params, function(xhr, status){
		if(status == "success"){
			done&& done(xhr)
		}

		
	})
}
function sendMsg(o){
	
	if(!o.conv_id){
		create(o, send);
	}
	else{
		send(o);
	}

}
function syncConversationList(id, done){
	var convs = [];
	var query = AVHelper.createQuery(ConvClass);
	query.equalTo("m", id);
//	query.ascending("latestMsg.timestamp");
	query.ascending("latestTimestamp");
	query.find().then(function(results) {
	  // Collect one promise for each delete into an array.
	  var promises = [];
	   
	    // 处理返回的结果数据
	    for (var i = 0; i < results.length; i++) {
	    	
	      var object = results[i];

	      var _conv ={
	      	conv_id:object.getObjectId(),
	      	m: object.get('m'),
	      	my_id: id,
	      	latestMsg: object.get('latestMsg') && JSON.parse(object.get('latestMsg')),
	      }
	      convs.push(_conv);
	      var promise = getPeerUser(_conv, function(conv, peer){

	      	conv.peerUser = peer;
//	      	console.log("find peer User: "+ JSON.stringify(conv))
	      	
	      })
	     
	      promises.push(promise);
	    }
	    
	  // Return a new promise that is resolved when all of the deletes are finished.
	  return AV.Promise.when(promises);
	
	},function(error){
		console.log("Error: " + error.code + " " + error.message);
	}).then(function() {
	  done&& done(convs)
	});
	

}

function getPeerUser(o, find){
	var peerIds = [];
	var members = o.m;
	for(var i in members){
		if(members[i] != o.my_id){
			peerIds.push(members[i])
		}
	}
	
	
	if(peerIds.length == 0){
		return null;
	}
	var peerId = peerIds[0];
	
	
	
	var query = new AV.Query(AV.User);
	query.equalTo("objectId", peerId);
//		query.equalTo("username", peerId); 
	return query.find({
		  success: function(peer) {
		  	peer = peer[0]
		  	if(peer){
			  	var peerUser = {
			  		userName:　peer.get('username'),
			  		userId:　peer.getObjectId(),
			  		userPhoto: peer.get('photo'),
			  		pushId: peer.get('pushId'),
			  		
			  	}
			    find && find(o, peerUser);
			}
		  },
		  error: function(){
			
		  }
	});
			
}

function saveLatestMsg(msg){
	AVHelper.getById(ConvClass, msg.conv_id, function(obj){
		var latestTimestamp = new Date().getTime();
		AVHelper.save(obj, {latestMsg: JSON.stringify(msg), latestTimestamp:latestTimestamp});
	})
}
function extend(target, value){
	for(var p in value){
		target[p] = value[p];
	}
}