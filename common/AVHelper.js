var AV = require('leanengine');
var crypto = require("../common/crypto")
var http = require('https')
var querystring = require('querystring')


//var APP_ID = process.env.LC_APP_ID || '2vqtxi2xvij6vb7xfempoze1bjvdgi1yum2e2xictilem1q9'; // your app id
//var APP_KEY = process.env.LC_APP_KEY || '1o9rb4nwxijae6i68q28gvgtoov3zb2rj84ml35ye2lqqlo6'; // your app key
//var MASTER_KEY = process.env.LC_APP_MASTER_KEY || '7od1rzoaahay7a5a5nm2p5wbqy65b6mxpvgj35sj8fj1ilky'; // your app master key

var APP_ID = '2vqtxi2xvij6vb7xfempoze1bjvdgi1yum2e2xictilem1q9'; // your app id
var APP_KEY =  '1o9rb4nwxijae6i68q28gvgtoov3zb2rj84ml35ye2lqqlo6'; // your app key
var MASTER_KEY =   '7od1rzoaahay7a5a5nm2p5wbqy65b6mxpvgj35sj8fj1ilky'; // your app master key

//AV.initialize(APP_ID, APP_KEY, MASTER_KEY);
//console.log("APP_ID" + APP_ID)
//console.log("APP_KEY" + APP_ID)
//console.log("MASTER_KEY" + MASTER_KEY)

function assign(target, value){
	for(var p in value){
		target.set(p, value[p]);
	}
}
var RestHelper = {
	post: function(url, data, success, master){
		RestRequest(url, JSON.stringify(data), "POST", success, master);
	},
	get:function(url, data,success, master){
		RestRequest(url, data, "GET", success, master);
	},
	put:function(url, data, success, master){
		RestRequest(url, JSON.stringify(data), "PUT", success, master);
	},
}

extend(exports , {
		use: function(o){
			if(this[o] == undefined){
				console.log("use Cloud Object: "+ o);
				this[o] = AV.Object.extend(o);
			}else{
				console.log("reuse Cloud Object: "+ o);
			}
			return this[o]
		},
		save:save,
		getById: queryObj,
		assign: assign,
		RestHelper: RestHelper,
		createQuery: function(o){
			return new AV.Query(o);
		},
		getRawAV: function(){return AV;}
});


function save(o,p, succ){

	p = p || null;
	succ = succ || function(post){/*console.log('save done' + JSON.stringify(p)); */};

	o.save(p,{
		 success: succ,
		 error: function(post, error) {
		    console.log('Failed to save object, with error message: ' + error.message);
		 },
	})
}

function queryObj(o, id, success){
	var query = new AV.Query(o);
		query.get(id,{
	  success: success,
	  error: function(object, error) {
	   console.log("queryId: "+ id + error.message)
	  }
	});
}

function extend(target, value){
	for(var p in value){
		target[p] = value[p];
	}
}



var default_success = function(response) {
//		if (dataType === 'json') {
//			response = JSON.stringify(response);
//		} else if (dataType === 'xml') {
//			response = new XMLSerializer().serializeToString(response).replace(/</g, "&lt;").replace(/>/g, "&gt;");
//		}
//		console.log("success: " + JSON.stringify(response));
 console.log('STATUS: ' + response.statusCode);
  console.log('HEADERS: ' + JSON.stringify(response.headers));
		require("./Util").displayProp(response)
		 response.setEncoding('utf8');
		  response.on('data', function (chunk) {
		    console.log('BODY: ' + chunk);
		  });
				
		return response;
	};
	

function RestRequest(url, data, type, success, master){
	
	var path = "/1.1"+url;
		if(type.toUpperCase() == "GET"){
			path = path + "?"+ querystring.stringify(data);
			data = null;
		}

		var dataType = 'json';
		success = success || default_success;
		
//		url = url + (dataType === 'html' ? 'text' : dataType);
		var heads = master? {
			"X-AVOSCloud-Application-Id":  APP_ID,
			"X-AVOSCloud-Request-Sign": (function(){
					var d = new Date().getTime();
					var sign = crypto.md5(d + MASTER_KEY);
					return sign +"," +d +",master";

			}()),
			"Content-Type": 'application/json',
		}:{
			"X-AVOSCloud-Application-Id":  APP_ID,
			"X-AVOSCloud-Application-Key": APP_KEY,
			"Content-Type": 'application/json',
		};
		

		httpRequest({
				host: "api.leancloud.cn",
				path: path,
				type : type,
				headers: heads,
//				data: JSON.stringify(data),
				data: data,
				success: success,
				dataType: dataType,
			})
}

function httpRequest(settings){
	
	var options = {
	  hostname: settings.host,
	  port: 443,
	  path: settings.path,
	  method: settings.type,
	  headers: settings.headers,
	};
	
	var req = http.request(options, function(response){
		var data ="";
		if(response.statusCode == 200 || response.statusCode==201){
			response.setEncoding('utf8');
			response.on('data', function (chunk) {
				data += chunk;
			    
			  });
			response.on('end', function (chunk) {
				settings.success(JSON.parse(data), "success");
			});
		}
		else{
			console.log(response.statusCode + " " + response.statusMessage)
		}
		
		
	});
	
	req.on('error', settings.error || function(e) {
	  console.log('problem with request: ' + e.message);
	});
	
	// write data to request body
	settings.data && req.write(settings.data);
	req.end();
}
