assign(exports , {
	log : console.log,
	displayProp: displayProp,
	assign: assign,
	
});


function displayProp(obj){    
    var names=[];       
    for(var name in obj){       
       names.push(name);  
    }  
    console.log(names);  
} 

function assign(target, value){
	for(var p in value){
		target[p] = value[p];
	}
};