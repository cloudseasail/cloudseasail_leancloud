var Cloud = require("./Cloud");
var App = require("./Util");
var AV = require("leanengine")
App.assign(exports, {

	SubmitRoute : SubmitRoute,
	QueryRouteMatch: QueryRouteMatch,
});

Cloud.require("Route");

function SubmitRoute(route){
	var cloud_route = new Cloud.Route();
	route.submit_date = new Date();
	Cloud.assign(cloud_route, route);
//	App.displayProp(cloud_route);
	Cloud.save(cloud_route);
	
};

function QueryRouteMatch(route, report){
	//TODO: match algo
	var handle = function(results){
		var list =[];
		for (var r in results){
			list.push(results[r].attributes)
		}
		report(list)
	}
	
	var query_role;
	if(route.role == "driver"){
		query_role = "passenger"
	}else{
		query_role = "driver"
	}
	var query = new AV.Query(Cloud.Route);
	query.equalTo("role", query_role);
	query.find({
	  success: handle,
	  error: function(error) {
	    alert("Error: " + error.code + " " + error.message);
	  }
	});
	
}
