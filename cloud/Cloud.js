var App = require("./Util");
var AV = require('leanengine');


function assign(target, value){
	for(var p in value){
		target.set(p, value[p]);
	}
}

App.assign(exports , {
		require: function(o){
			if(this[o] == undefined){
				console.log("use Cloud Object: "+ o);
				this[o] = AV.Object.extend(o);
			}else{
				console.log("reuse Cloud Object: "+ o);
			}
		},
		save:save,
		getById: queryObj,
		assign: assign,
});


function save(o,p, succ){

	p = p || null;
	succ = succ || function(post){console.log('save done'); };

	o.save(p,{
		 success: succ,
		 error: function(post, error) {
		    console.log('Failed to create new object, with error message: ' + error.message);
		 },
	})
}

function queryObj(o, id, success){
	var query = new AV.Query(o);
		query.get(id,{
	  success: success,
	  error: function(object, error) {
	   console.log(error.message)
	  }
	});
}