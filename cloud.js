var AV = require('leanengine');
var Route = require("./cloud/Route")
var IMWrapper = require("./LeanIM/wrapper")
var Util = require("./common/Util")
/**
 * 一个简单的云代码方法
 */
AV.Cloud.define('hello', function(request, response) {
  response.success('Hello world!');
});

AV.Cloud.define('SubmitRoute', function(request, response) {
  Route.SubmitRoute(request.params);
  response.success("OK");
});

AV.Cloud.define('QueryRouteMatch', function(request, response) {
  Route.QueryRouteMatch(request.params, function(r){
  	response.success(r);
  });
})

AV.Cloud.define('findUser', function(request, response) {
	  var query = new AV.Query(AV.User);
	query.equalTo("objectId", request.params.userId);  // find all the women
	query.find({
	  success: function(u) {
	  	u = u[0];
	  	console.log(u)
	   response.success({name:u.get("username"), pass:u.get("password"), phone: u.get("mobilePhoneNumber")});
	  },
	  error: function(e){
	  	response.error(e);
	  }
	  	
	});

});

AV.Cloud.define('LeanIMmain', function(request, response) {
    IMWrapper.main(function(m){
    	
    	response.success({msg: m});
    }, function(m){
    	response.error({msg: m});
console.log("LeanIMmain error " +m)
    });
})

// Cloud IM


var CloudIM = require("./CloudIM/wrapper")
AV.Cloud.define('CloudIM_CreateConversation', function(request, response) {
	CloudIM.createConversation(request.params, function(conv){
		
//		console.log("CloudIM_CreateConversation success " + JSON.stringify(conv))
		response.success(conv)
	})
})
AV.Cloud.define('CloudIM_getHistoryMessages', function(request, response) {
	CloudIM.getHistoryMessages(request.params, function(array){
		
//		console.log("CloudIM_getHistoryMessages success " + JSON.stringify(array))
		response.success(array)
	})
})
AV.Cloud.define('CloudIM_sendMsg', function(request, response) {
	CloudIM.sendMsg(request.params, function(msg){
		
//		console.log("CloudIM_sendMsg success " + JSON.stringify(msg))
		response.success(msg)
//		response.success("done")
		
	})
})
AV.Cloud.define('CloudIM_syncConversationList', function(request, response) {
	CloudIM.syncConversationList(request.params.my_id, function(array){
		
//		console.log("CloudIM_syncConversationList success " + JSON.stringify(array))
//		response.success(msg)
		response.success(array)
		
	})
})
AV.Cloud.define('CloudIM_newUserRegistration', function(request, response) {
	CloudIM.newUserRegistration(request.params, function(o){
		
//		console.log("CloudIM_syncConversationList success " + JSON.stringify(array))
//		response.success(msg)
		response.success(o)
		
	})
})


module.exports = AV.Cloud;
