var realtime = require('leancloud-realtime');
//var AV = require('leanengine');
realtime.config({
  WebSocket: require('ws')
});

exports.main = main;

var appId = '2vqtxi2xvij6vb7xfempoze1bjvdgi1yum2e2xictilem1q9';

// 请换成你自己的一个房间的 conversation id（这是服务器端生成的）
var roomId = '551a2847e4b04d688d73dc54';
// 每个客户端自定义的 id
var clientId = 'LeanCloud';

// 用来存储 realtimeObject
var rt;

// 用来存储创建好的 roomObject
var room;

// 监听是否服务器连接成功
var firstFlag = true;

// 用来标记历史消息获取状态
var logFlag = false;

function showLog(m){
	console.log(m);
}

function main(passH, errorH) {
  showLog('正在链接服务器，请等待。。。');

  if (!firstFlag) {
    rt.close();
  }

  // 创建实时通信实例
  rt = realtime({
    appId: appId,
    clientId: clientId,

    // 请注意，这里关闭 secure 完全是为了 Demo 兼容范围更大些
    // 具体请参考实时通信文档中的「其他兼容问题」部分
    // 如果真正使用在生产环境，建议不要关闭 secure，具体阅读文档
    // secure 设置为 true 是开启
    secure: false
  });

  // 监听连接成功事件
  rt.on('open', function() {
    firstFlag = false;
    showLog('服务器连接成功！');

    // 获得已有房间的实例
    rt.room(roomId, function(object) {

      // 判断服务器端是否存在这个 room，如果存在
      if (object) {
        room = object;

        // 当前用户加入这个房间
        room.join(function() {

          // 获取成员列表
          room.list(function(data) {
            showLog('当前 Conversation 的成员列表：', data);

            // 获取在线的 client（Ping 方法每次只能获取 20 个用户在线信息）
            rt.ping(data.slice(0, 20), function(list) {
              showLog('当前在线的成员列表：', list);
            });

            var l = data.length;

            // 如果超过 500 人，就踢掉一个。
            if (l > 490) {
              room.remove(data[30], function() {
                showLog('人数过多，踢掉： ', data[30]);
              });
            }

            // 获取聊天历史
            getLog(function() {
              printWall.scrollTop = printWall.scrollHeight;
              showLog('已经加入，可以开始聊天。');
            });
          });

        });

        // 房间接受消息
        room.receive(function(data) {
          if (!msgTime) {
            // 存储下最早的一个消息时间戳
            msgTime = data.timestamp;
          }
          showMsg(data);
        });
      } else {
        // 如果服务器端不存在这个 conversation
        showLog('服务器不存在这个 conversation，你需要创建一个。');

        // 创建一个新 room
        rt.room({
          // Room 的默认名字
          name: 'LeanCloud-Room',

          // 默认成员的 clientId
          members: [
            // 当前用户
            clientId
          ],
          // 创建暂态的聊天室（暂态聊天室支持无限人员聊天，但是不支持存储历史）
          // transient: true,
          // 默认的数据，可以放 Conversation 名字等
          attr: {
            test: 'demo2'
          }
        }, function(obj) {

          // 创建成功，后续你可以将 room id 存储起来
          room = obj;
          roomId = room.id;
          showLog('创建一个新 Room 成功，id 是：', roomId);

          // 关闭原连接，重新开启新连接
          rt.close();
          main();
        });
      }
    });
    
    passH('服务器连接成功！');
  });

  // 监听服务情况
  rt.on('reuse', function() {
    errorH('服务器正在重连，请耐心等待。。。');
  });

  // 监听错误
  rt.on('error', function(data) {
    errorH('连接遇到错误。。。');
  });
}

